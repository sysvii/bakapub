#!/usr/bin/env ruby

require 'nokogiri'
require 'http'
require 'docopt'
require 'fileutils'
require 'open-uri'

version = "0.1.0"
BASE_URL = "https://www.baka-tsuki.org"
doc = <<DOCOPT
Bakapub #{version}

Usage:
  #{__FILE__} <url> [--out=<path>]
  #{__FILE__} --version

  Options:
  -h --help   Show this screen
--version   Show version

DOCOPT

def process(url, outDir)

  FileUtils::mkdir_p outDir

  htmldoc = Nokogiri::HTML(HTTP.get(url).to_s)

  series = url.match(/\?title=(.*?):/).captures.first
  vol = url.match(/Volume_(\d+\.?\d*)/).captures.first
  list = []
  refCount = 1

  # setup meta data
  puts "Make refdoc"
  refDoc = File.new("#{outDir}#{File::SEPARATOR}_references.md", "w")
  refDoc.write("## References\n\n")

  puts "Try to get cover"
  begin
    downloadImage("/project/index.php?title=File:#{series}_#{vol.to_s.rjust(2, "0")}.jpg", "#{outDir}#{File::SEPARATOR}cover.jpg")
    puts "Got it"
  rescue
  end

  # Metadata & title
  File.open("#{outDir}#{File::SEPARATOR}_title.md", "w") do |f|
    f << "% #{series} Volume #{vol}\n"
    f << "% \n"
    list << "_title.md"
  end

  loop do
    #
    chap = url.match(/Chapter_(\d+-?\d*)/)

    if chap
      chap = chap.captures.first
    else
      chap = ""
    end

    puts "vol:" + vol.to_s + " chap:" + chap.to_s

    text = htmldoc.search("[@id=mw-content-text]")
    title = text.at_css('span.mw-headline').text
    table = text.at_css('ol.references')


    doc = File.new("#{outDir}#{File::SEPARATOR}#{title}.md", "w")
    list << "#{title}.md"

    doc << "# " + title + "\n\n"

    text.css("li.gallerybox").each do |li|
      img = li.at_css("a.image").attr "href"
      fname = img.split(":")[-1]
      puts "Getting Illustraion"
      downloadImage(img, "#{outDir}#{File::SEPARATOR}#{fname}")
      doc << "![](#{fname})\n\n"
    end

    # TODO: Ground work to change to children approach
    # text.children.each do |ele|
    #   puts "ele:" + ele.name
    # end

    # TODO: Refactor & place in children loop
    text.xpath("//p").each do |x|
      passage = x.to_s
      passage = passage.gsub(/(<p>)/, "")
      passage = passage.gsub(/(<\/?br>|<\/p>)/, "\n")
      passage = passage.gsub(/(<i>|<\/i>)/, "*")
      passage = passage.gsub(/(<\/?strong>|<\/?b>)/, "**")
      if x.at_css('sup.reference')
        x.css('sup.reference').each do |r|
          tableId = r.attr("id").gsub(/ref/, "note")
          refText = table.at_css("#" + tableId).at_css("span.reference-text").text
          refDoc.write("[\^#{refCount}]: #{refText}\n\n")
          passage[r.to_s] = "[^#{refCount}]"
          puts "Reference found ##{refCount}"
          refCount += 1
        end
      end
      # puts passage
      doc << passage + "\n"
    end

    doc << "\n\\pagebreak\n"
    doc.close()

    # Keep going to the next chapter
    nextPage = text.xpath("//td")[-1]
    if nextPage and nextPage.text.include? "Forward"
      # Keep going
      vol = url.match(/Volume_(\d+)/).captures.first
      url = nextPage.search("a").attr('href').to_s
      newVol = url.match(/Volume_(\d+)/).captures.first
      # FIXME: assumes domain relative url
      if newVol != vol then
        break
      end
      url = "http://www.baka-tsuki.org" + url
      htmldoc = Nokogiri::HTML(HTTP.get(url).to_s)
    else
      break
    end
  end
  list << "_references.md"
  generate_make(list, outDir, "#{series}-#{vol}.epub")
end

def downloadImage(url, path)
  # Handle relative links
  if url[0] == '/'
    url = BASE_URL + url
  end

  # Try to go a level deeper
  begin
    htmldoc = Nokogiri::HTML(HTTP.get(url).to_s)
    url = BASE_URL + htmldoc.css("div.fullMedia a.internal").first.attr('href')
  rescue
  end

  File.open(path, "wb") do |saved_file|
    open(url, "rb") do |read_file|
      saved_file.write(read_file.read)
    end
  end
end

def generate_make(list, outDir, outName)
  make = File.new("#{outDir}#{File::SEPARATOR}makefile", "w")
  make << "
CC:\=pandoc
ARGS:\= -f markdown+footnotes+yaml_metadata_block --epub-cover=cover.jpg
IN:\= #{list.map{|x| "\"" + x.gsub(/\"/, "\\\"") + "\"" }.join ' '}
OUT:\=#{outName}

all: $(OUT)

$(OUT):
\t$(CC) $(ARGS) $(IN) -o $@

clean:
\trm $(OUT)
"
end

begin
  opt = Docopt::docopt(doc)
  puts opt

  if opt["--version"]
    puts version
  elsif opt["<url>"]
    process(opt["<url>"], opt["--out"])
  end

rescue Docopt::Exit => e
  puts e.message
end
