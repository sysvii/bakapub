# Bakapub

A script to download and convert [baka-tsuki](https://www.baka-tsuki.org) translation pages into pandoc
compliant markdown

## Usage

To download the first volume of Hyouka run:

> `bakapub "https://www.baka-tsuki.org/project/index.php?title=Hyouka:Volume_1_Chapter_1"`

Output files are written into `out/`

or

> `bakapub "https://www.baka-tsuki.org/project/index.php?title=Hyouka:Volume_1_Chapter_1" --out=vol1`

Output files are written into `vol1/`

### Building to epub

The script generates a makefile that could without modification build an epub.

To use the makefile run `make` in the output directory.

### Building other formats

To build other formats like PDF, or any other format supported by `pandoc`
edit the generated makefile

## Notes for manual `pandoc` compilation

* Use `footnotes` extension for any references

* include the cover by using `--epub-cover=<PATH TO COVER>`

* `_title.md` should be first and `_references.md` the last files in the list

## TODO

* ~~build epub with pandoc in script~~ (generates a makefile)

* ~~title page~~

* generate epub metadata

* ~~inline images~~ sort of

* ~~specify output directory~~

* get all volumes from a series

* ~~cover image~~

* a half decent CLI

* expose pandoc features like epub style sheet, etc.

* restructure so parse so all elements are in order as they are on the website
